from aiohttp import web
from webargs import aiohttpparser

from src.api.status.schemas import (
    SiteStatusRequest,
    SiteStatusRequestSchema,
    SiteStatusResponse,
    SiteStatusResponseSchema,
)


async def check_site_status(request: web.Request) -> web.Response:
    """Проверка статуса сайта.

    ---
    post:
      description: Проверка статуса сайта.
      requestBody:
        required: true
        content:
          application/json:
            schema: SiteStatusRequest
      responses:
        200:
          description: OK
          content:
            application/json:
              schema: SiteStatusResponse
        422:
          description: Запрос не соответствует схеме.
    """
    status_request: SiteStatusRequest = await aiohttpparser.parser.parse(
        argmap=SiteStatusRequestSchema,
        req=request,
        location='json',
    )
    status = SiteStatusResponse(
        http_status=None,
        error=False,
    )
    try:
        async with request.app['http_cli'].get(
            status_request.url,
            timeout=status_request.timeout,
        ) as resp:
            status.http_status = resp.status
    except Exception:
        status.error = True
    return web.json_response(SiteStatusResponseSchema().dump(status))
